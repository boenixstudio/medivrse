﻿using UnityEngine;

public class Instantiater : MonoBehaviour {
    [SerializeField] GameObject[] objects;
    [SerializeField] Transform parentTransform;
    [SerializeField] float delay=8f;

	// Use this for initialization
	void Start () {
        InvokeRepeating("InstantiateObject",1, delay);
    }
	

    private void InstantiateObject()
    {
        Instantiate(objects[Random.Range(0, objects.Length - 1)], parentTransform.position, Quaternion.identity, parentTransform);
    }
}